<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>

        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Learning Path</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <%@include file="../navigator/header.jsp" %>
    <body id="bg">
        <!-- <div class="page-wraper"> -->

        <!-- <div id="loading-icon-bx"></div> -->
        <!-- Header Top ==== -->




        <div class="container" style="margin-top:155px">

            <h3>Show Learning Path of a Subject</h3>
            <div class="mb-4">
                <form action="${pageContext.request.contextPath}/FLM/learningpath" method="get">
                    <div class="input-group input-group-sm">
                        <c:choose>
                            <c:when test="${txt.isEmpty()||txt==null}">
                                <input  type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Enter Subject Code..." name="txt">
                            </c:when>
                            <c:otherwise>
                                <input  type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="${txt}" name="txt">
                            </c:otherwise>

                        </c:choose>

                        <div class="input-group-append">
                            <button type="submit" class="btn btn-secondary btn-number">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>    
            </div>
            <c:choose>

                <c:when test="${list.isEmpty()||list==null}">
                    <h5>${message}</h5>
                </c:when>

                <c:otherwise>
                    <table class="table table-bordered ">
                        <thead class="table-warning">
                            <tr>
                                <th scope="col">Syllabus ID
                                    <c:if  test="${empty(sort)}">
                                        <small>
                                            <a href="learningpath?txt=${txt}&sort=iddesc&page=${page}"><i class="fa-solid fa-chevron-down"></i></a>
                                        </small> 
                                    </c:if>
                                    <c:if test="${sort == 'iddesc'}">
                                        <small>
                                            <a href="learningpath?txt=${txt}&sort=idasc&page=${page}"><i class="fa-solid fa-chevron-up"></i></a>
                                        </small> 
                                    </c:if>
                                    <c:if  test="${sort == 'idasc'}">
                                        <small>
                                            <a href="learningpath?txt=${txt}&sort=iddesc&page=${page}"><i class="fa-solid fa-chevron-down"></i></a>
                                        </small> 
                                    </c:if>
                                </th>
                                <th scope="col">Subject Name</th>
                                <th scope="col">Syllabus Name</th>
                                <th scope="col">DecisionNo MM/dd/yyyy</th>
                                <th scope="col">All subjects need to learn before</th>
                            </tr>
                        </thead>
                        <c:forEach items="${list}" var="ls">
                            <tr>
                                <th>${ls.getSyllabusID()}</th>
                                <td>${ls.getSubjectName()}</td>
                                <td><a href="${pageContext.request.contextPath}/FLM/syllabusdetails?id=${ls.getSyllabusID()}"' class="text-primary stretched-link">${ls.getSyllabusName()}</a></td>
                                <td>${ls.getDecision()}</td>
                                <td>
                                    <c:choose>
                                        <c:when test="${ls.getSubjectNeed().get(0).getPre().isEmpty()}">
                                            <h6>${ls.getSubjectNeed().get(0).getSubject()}: No Pre-Requisite</h6>
                                        </c:when>

                                        <c:otherwise>
                                            <c:forEach items="${ls.getSubjectNeed()}" var="ls2">
                                                <h6>${ls2.getSubject()}: ${ls2.getPre()}</h6>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                        </c:forEach>
                        <tbody>
                        </tbody>
                    </table>

                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-start">
                            <li class="page-item ${page == 1 ? 'disabled' : ''}">
                                <a class="page-link" href="learningpath?page=${page - 1}${txt != null ? '&txt=' : ''}${txt != null ?txt:''}&sort=${sort}">Pre</a>
                            </li>
                            <c:forEach begin="1" end="${numberOfPage}" var="i">
                                <li class="page-item ${page == i ? 'active' : ''}"><a class="page-link" href="learningpath?page=${i}${txt != null ? '&txt=' : ''}${txt != null ?txt:''}&sort=${sort}">${i}</a></li>

                            </c:forEach>
                            <li class="page-item ${page == numberOfPage ? 'disabled' : ''}">
                                <a class="page-link" href="learningpath?page=${page + 1}${txt != null ? '&txt=' : ''}${txt != null ?txt:''}&sort=${sort}">Next</a>
                            </li>
                        </ul>
                    </nav>

                </c:otherwise>
            </c:choose>
        </div>
        <c:choose>
            <c:when test="${list.isEmpty()||list==null}">
                <div style="margin-top: 750px"></div>
            </c:when>
            <c:otherwise>
                <div style="margin-top: 150px"></div>
            </c:otherwise>

        </c:choose>

    </body>
    <%@include file="../navigator/footer.jsp" %>
</html>

