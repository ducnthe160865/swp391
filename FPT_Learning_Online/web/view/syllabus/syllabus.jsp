<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>

        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Syllabus Management</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <%@include file="../navigator/header.jsp" %>
    <body id="bg">
        <div class="container" style="margin-top: 155px">
            <h3>Syllabus Listing</h3>
            <div class="mb-4">
                <form action="${pageContext.request.contextPath}/FLM/syllabus" method="get">
                    <div class="input-group input-group-sm">

                        <table class="auto-style1 mb-3">
                            <tbody><tr>
                                    <th class="auto-style13" style="text-align: right">Search by Subject:	

                                    </th>
                                    <td class="auto-style10">
                                        <div class="form-inline">
                                            <div class="input-group">
                                                <c:choose>
                                                    <c:when test="${txt.isEmpty()||txt==null}">
                                                        <input  type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Enter Subject Code or Name..." name="txt"style=width:500px;>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <input  type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="${txt}" name="txt"style=width:500px;>
                                                    </c:otherwise>

                                                </c:choose>
                                                <div class="input-group-append">
                                                    <button type="submit" name="chon" class="btn btn-secondary btn-number">
                                                        <i class="fa fa-search"></i>
                                                    </button>       
                                                </div>
                                            </div>
                                    </td>
                                </tr>
                            </tbody></table>


                    </div>
            </div>
        </form>    
    </div>
    <div class="container mt-5">
        <c:choose>

            <c:when test="${list.isEmpty()||list==null}">
                <h5>${message}</h5>
            </c:when>
            <c:otherwise>
                <table class="table table-bordered table-striped table-hover">
                    <thead class="table-warning">
                        <tr>
                            <th scope="col"> ID
                                <c:if  test="${empty(sort)|| sort == 'codedesc' || sort == 'codeasc'|| requestScope.sort == 'typedesc' || requestScope.sort == 'typeasc'}">
                                    <small>
                                        <a href="syllabus?txt=${txt}&sort=iddesc&page=${page}"><i class="fa-solid fa-chevron-down"></i></a>
                                    </small> 
                                </c:if>
                                <c:if test="${sort == 'iddesc'}">
                                    <small>
                                        <a href="syllabus?txt=${txt}&sort=idasc&page=${page}"><i class="fa-solid fa-chevron-up"></i></a>
                                    </small> 
                                </c:if>
                                <c:if  test="${sort == 'idasc'}">
                                    <small>
                                        <a href="syllabus?txt=${txt}&sort=iddesc&page=${page}"><i class="fa-solid fa-chevron-down"></i></a>
                                    </small> 
                                </c:if>
                            </th>
                            <th scope="col">Subject Code
                             <c:if  test="${empty(sort)|| requestScope.sort == 'iddesc' || requestScope.sort == 'idasc'|| requestScope.sort == 'typedesc' || requestScope.sort == 'typeasc'}">
                                    <small>
                                        <a href="syllabus?txt=${txt}&sort=codedesc&page=${page}"><i class="fa-solid fa-chevron-down"></i></a>
                                    </small> 
                                </c:if>
                                <c:if test="${sort == 'codedesc'}">
                                    <small>
                                        <a href="syllabus?txt=${txt}&sort=codeasc&page=${page}"><i class="fa-solid fa-chevron-up"></i></a>
                                    </small> 
                                </c:if>
                                <c:if  test="${sort == 'codeasc'}">
                                    <small>
                                        <a href="syllabus?txt=${txt}&sort=codedesc&page=${page}"><i class="fa-solid fa-chevron-down"></i></a>
                                    </small> 
                                </c:if>
                            </th>
                            <th scope="col">Subject Name
                                 <c:if  test="${empty(sort)|| requestScope.sort == 'iddesc' || requestScope.sort == 'idasc'|| sort == 'codedesc' || sort == 'codeasc'}">
                                    <small>
                                        <a href="syllabus?txt=${txt}&sort=typedesc&page=${page}"><i class="fa-solid fa-chevron-down"></i></a>
                                    </small> 
                                </c:if>
                                <c:if test="${sort == 'typedesc'}">
                                    <small>
                                        <a href="syllabus?txt=${txt}&sort=typeasc&page=${page}"><i class="fa-solid fa-chevron-up"></i></a>
                                    </small> 
                                </c:if>
                                <c:if  test="${sort == 'typeasc'}">
                                    <small>
                                        <a href="syllabus?txt=${txt}&sort=typedesc&page=${page}"><i class="fa-solid fa-chevron-down"></i></a>
                                    </small> 
                                </c:if>
                            </th>
                            <th scope="col">Syllabus Name</th>
                            <th scope="col">IsApproved</th>
                            <th scope="col">IsActive</th>
                            <th scope="col">DecisionNo MM/dd/yyyy</th>
                        </tr>
                    </thead>

                 
                        <c:forEach items="${list}" var="o">
                            <tr>
                                <td>${o.getSyllabusId()}</td>
                                <th>${o.getSubjectCode()}</th>
                                <td><a href="${pageContext.request.contextPath}/FLM/syllabusdetails?id=${o.getSyllabusId()}">${o.getSyllabusNameEn()}</a></td>
                                <td><a href="${pageContext.request.contextPath}/FLM/syllabusdetails?id=${o.getSyllabusId()}">${o.getSyllabusNameVn()}</a></td>
                                <td><input type="checkbox" <c:if test="${o.isActive}">checked</c:if> disabled=""></td>
                                <td><input type="checkbox" <c:if test="${o.isApproved}">checked</c:if> disabled=""></td>
                                <th>${o.getDecisionNo()}</th>


                            </tr>
                        </c:forEach>
                   
                </table>
             
                <!--Paging Start-->

                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-start">
                            <li class="page-item ${page == 1 ? 'disabled' : ''}">
                                <a class="page-link" href="syllabus?page=${page - 1}${txt != null ? '&txt=' : ''}${txt != null ?txt:''}&sort=${sort}">Pre</a>
                            </li>
                            <c:forEach begin="1" end="${numberOfPage}" var="i">
                                <li class="page-item ${page == i ? 'active' : ''}"><a class="page-link" href="syllabus?page=${i}${txt != null ? '&txt=' : ''}${txt != null ?txt:''}&sort=${sort}">${i}</a></li>

                            </c:forEach>
                            <li class="page-item ${page == numberOfPage ? 'disabled' : ''}">
                                <a class="page-link" href="syllabus?page=${page + 1}${txt != null ? '&txt=' : ''}${txt != null ?txt:''}&sort=${sort}">Next</a>
                            </li>
                        </ul>
                    </nav>
       
                <!--Paging End-->
            </c:otherwise>
        </c:choose>

    </div>
<c:choose>
            <c:when test="${list.isEmpty()||list==null}">
                <div style="margin-top: 750px"></div>
            </c:when>
            <c:otherwise>
                <div style="margin-top: 150px"></div>
            </c:otherwise>

        </c:choose>
</body>
<%@include file="../navigator/footer.jsp" %>
</html>

