<%-- 
    Document   : curriadmin
    Created on : Feb 17, 2023, 12:28:26 AM
    Author     : inuya
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-list-ul" viewBox="0 0 16 16">
    <path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm-3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
    </svg>
    <!-- OG -->
    <meta property="og:title" content="EduChamp : Education HTML Template" />
    <meta property="og:description" content="EduChamp : Education HTML Template" />
    <meta property="og:image" content="" />
    <meta name="format-detection" content="telephone=no">

    <!-- FAVICONS ICON ============================================= -->
    <link rel="icon" href=".error-404.html" type="image/x-icon" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png" />

    <!-- PAGE TITLE HERE ============================================= -->
    <title>EduChamp : Education HTML Template </title>

    <!-- MOBILE SPECIFIC ============================================= -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--[if lt IE 9]>
    <script src="../assets/js/html5shiv.min.js"></script>
    <script src="../assets/js/respond.min.js"></script>
    <![endif]-->

    <!-- All PLUGINS CSS ============================================= -->
    <link rel="stylesheet" type="text/css" href="../assets/css/assets.css">
    <link rel="stylesheet" type="text/css" href="../assets/vendors/calendar/fullcalendar.css">

    <!-- TYPOGRAPHY ============================================= -->
    <link rel="stylesheet" type="text/css" href="../assets/css/typography.css">

    <!-- SHORTCODES ============================================= -->
    <link rel="stylesheet" type="text/css" href="../assets/css/shortcodes/shortcodes.css">

    <!-- STYLESHEETS ============================================= -->
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/dashboard.css">
    <link class="skin" rel="stylesheet" type="text/css" href="../assets/css/color/color-1.css">
    <style>
        .sort-btn {
            border: none;
            background-color: transparent;
            cursor: pointer;
            padding: 0;
            margin: 0;
        }

        .sort-btn i {
            margin-left: 5px;
            color: #999;
            transition: color 0.3s ease-in-out;
        }

        .sort-btn.active i {
            color: #000;
        }

    </style>
</head>
<body class="ttr-opened-sidebar ttr-pinned-sidebar">

    <!-- header start -->
    <jsp:include page="../Common/heading/heading.jsp"/>
    <!-- header end -->
    <!-- Left sidebar menu start -->
    <jsp:include page="../Common/slideBar/left.jsp" />
    <!-- Left sidebar menu end -->

    <!--Main container start -->
    <main class="ttr-wrapper">
        <div class="container-fluid">
            <div class="db-breadcrumb">
                <h4 class="breadcrumb-title">Syllabus List</h4>
            </div>	
            <div class="row">
                <!-- Your Profile Views Chart -->
                <div class="col-lg-12 m-b30">
                    <form action="${pageContext.request.contextPath}/FLM/syllabuslist1" method="post">
                        <div class="widget-box">
                            <table class="auto-style1 mb-3">
                                <tbody><tr>
                                        <th class="auto-style13" style="text-align: right">Search by Syllabus:	

                                        </th>
                                        <td class="auto-style10">
                                            <div class="form-inline">
                                                <div class="input-group">
                                                    <c:choose>
                                                        <c:when test="${txt.isEmpty()||txt==null}">
                                                            <input  type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Enter Subject Code or Name..." name="txt"style=width:500px;>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <input  type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="${txt}" name="txt"style=width:500px;>
                                                        </c:otherwise>

                                                    </c:choose>
                                                    <div class="input-group-append">
                                                        <button type="submit" name="chon" class="btn btn-secondary btn-number">
                                                            <i class="fa fa-search"></i>
                                                        </button>       
                                                    </div>
                                                </div>
                                        </td>
                                    </tr>
                                </tbody></table>
                    </form>    

                    <a  href="syllabusAdd" class="btn " role="button" aria-pressed="true">Add New</a>
                    <div class="widget-inner">

                        <div class="table-responsive mt-4" >
                            <form action="${pageContext.request.contextPath}/FLM/syllabuslist1" method="get">
                                <table class="table">
                                    <caption>List of Syllabus</caption>
                                    <thead>
                                        <tr>
                                            <th >ID
                                                <c:if  test="${empty(sort)|| sort == 'codedesc' || sort == 'codeasc'|| requestScope.sort == 'typedesc' || requestScope.sort == 'typeasc'}">
                                                    <small>
                                                        <a href="syllabuslist1?txt=${txt}&sort=iddesc&page=${page}"><i class="fa-solid fa-chevron-down"></i></a>
                                                    </small> 
                                                </c:if>
                                                <c:if test="${sort == 'iddesc'}">
                                                    <small>
                                                        <a href="syllabuslist1?txt=${txt}&sort=idasc&page=${page}"><i class="fa-solid fa-chevron-up"></i></a>
                                                    </small> 
                                                </c:if>
                                                <c:if  test="${sort == 'idasc'}">
                                                    <small>
                                                        <a href="syllabuslist1?txt=${txt}&sort=iddesc&page=${page}"><i class="fa-solid fa-chevron-down"></i></a>
                                                    </small> 
                                                </c:if>
                                            </th>
                                            <th class="text-center">Subject
                                                <c:if  test="${empty(sort)|| requestScope.sort == 'iddesc' || requestScope.sort == 'idasc'|| requestScope.sort == 'typedesc' || requestScope.sort == 'typeasc'}">
                                                    <small>
                                                        <a href="syllabuslist1?txt=${txt}&sort=codedesc&page=${page}"><i class="fa-solid fa-chevron-down"></i></a>
                                                    </small> 
                                                </c:if>
                                                <c:if test="${sort == 'codedesc'}">
                                                    <small>
                                                        <a href="syllabuslist1?txt=${txt}&sort=codeasc&page=${page}"><i class="fa-solid fa-chevron-up"></i></a>
                                                    </small> 
                                                </c:if>
                                                <c:if  test="${sort == 'codeasc'}">
                                                    <small>
                                                        <a href="syllabuslist1?txt=${txt}&sort=codedesc&page=${page}"><i class="fa-solid fa-chevron-down"></i></a>
                                                    </small> 
                                                </c:if>
                                            </th>
                                            <th class="text-center">Syllabus Name
                                                <c:if  test="${empty(sort)|| requestScope.sort == 'iddesc' || requestScope.sort == 'idasc'|| sort == 'codedesc' || sort == 'codeasc'}">
                                                    <small>
                                                        <a href="syllabuslist1?txt=${txt}&sort=typedesc&page=${page}"><i class="fa-solid fa-chevron-down"></i></a>
                                                    </small> 
                                                </c:if>
                                                <c:if test="${sort == 'typedesc'}">
                                                    <small>
                                                        <a href="syllabuslist1?txt=${txt}&sort=typeasc&page=${page}"><i class="fa-solid fa-chevron-up"></i></a>
                                                    </small> 
                                                </c:if>
                                                <c:if  test="${sort == 'typeasc'}">
                                                    <small>
                                                        <a href="syllabuslist1?txt=${txt}&sort=typedesc&page=${page}"><i class="fa-solid fa-chevron-down"></i></a>
                                                    </small> 
                                                </c:if>  
                                            </th>
                                            <th class="text-center">IsApproved</th>
                                            <th class="text-center">IsActive</th>
                                            <th class="text-center">DecisionNo</th>
<!--                                            <th class="text-center">Designer</th>-->
                                            <th class="text-center">Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${list}" var="o">
                                            <tr>
                                                <td scope="row" class="text-center">${o.getSyllabusId()}</td>
                                                <th >${o.getSubjectCode()}</th>                                     
                                                <td class="text-center">${o.getSyllabusNameEn()}</td>   
                                                <td class="text-center"><i class="fa ${o.isApproved ? 'fa-check' : 'fa-times'} ${o.isApproved ? 'text-success' : 'text-danger'}"></i></td>
                                                <td class="text-center"><i class="fa ${o.isActive ? 'fa-check' : 'fa-times'} ${o.isActive ? 'text-success' : 'text-danger'}"></i></td>

                                                <td class="text-center">${o.getDecisionNo()}</td>
                                                <td class="text-center"></td>
                                                <td class="text-center"><a class="bi bi-list-ul"href="${pageContext.request.contextPath}/FLM/detailsyllabus?syllabusId=${o.syllabusId}"></a></td>

                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>



                                <!--Paging Start-->
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination justify-content-start">
                                        <li class="page-item ${page == 1 ? 'disabled' : ''}">
                                            <a class="page-link" href="syllabuslist1?page=${page - 1}${txt != null ? '&txt=' : ''}${txt != null ?txt:''}&sort=${sort}">Pre</a>
                                        </li>
                                        <c:forEach begin="1" end="${numberOfPage}" var="i">
                                            <li class="page-item ${page == i ? 'active' : ''}"><a class="page-link" href="syllabuslist1?page=${i}${txt != null ? '&txt=' : ''}${txt != null ?txt:''}&sort=${sort}">${i}</a></li>

                                        </c:forEach>
                                        <li class="page-item ${page == numberOfPage ? 'disabled' : ''}">
                                            <a class="page-link" href="syllabuslist1?page=${page + 1}${txt != null ? '&txt=' : ''}${txt != null ?txt:''}&sort=${sort}">Next</a>
                                        </li>
                                    </ul>
                                </nav>
                                <!--Paging End-->

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>

<script>
//            function sort(sort) {
//                let search = document.getElementById('search');
//                document.form1.action = 'decisionList?sort=' + sort + "&search=" + search.value;
//
//            }

</script>
<!--<div class="ttr-overlay"></div>-->

<!-- External JavaScripts -->
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/vendors/bootstrap/js/popper.min.js"></script>
<script src="../assets/vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="../assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
<script src="../assets/vendors/magnific-popup/magnific-popup.js"></script>
<script src="../assets/vendors/counter/waypoints-min.js"></script>
<script src="../assets/vendors/counter/counterup.min.js"></script>
<script src="../assets/vendors/imagesloaded/imagesloaded.js"></script>
<script src="../assets/vendors/masonry/masonry.js"></script>
<script src="../assets/vendors/masonry/filter.js"></script>
<script src="../assets/vendors/owl-carousel/owl.carousel.js"></script>
<script src='../assets/vendors/scroll/scrollbar.min.js'></script>
<script src="../assets/js/functions.js"></script>
<script src="../assets/vendors/chart/chart.min.js"></script>
<script src="../assets/js/admin.js"></script>
<script src='../assets/vendors/switcher/switcher.js'></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        if (document.getElementById('message').innerHTML !== '') {
            $('.toast').toast('show');
        }
    });
</script>
</body>
</html> 