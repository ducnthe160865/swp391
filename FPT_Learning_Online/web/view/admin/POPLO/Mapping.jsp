<%-- 
    Document   : userlist1
    Created on : Feb 26, 2023, 11:18:38 PM
    Author     : HP
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href=".error-404.html" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>User List Management </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="../assets/js/html5shiv.min.js"></script>
        <script src="../assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="../assets/css/assets.css">
        <link rel="stylesheet" type="text/css" href="../assets/vendors/calendar/fullcalendar.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="../assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="../assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="../assets/css/dashboard.css">
        <link class="skin" rel="stylesheet" type="text/css" href="../assets/css/color/color-1.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="../view/toast/toast.css"/>
        <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
            crossorigin="anonymous"
            />
    </head>
    <body class="ttr-opened-sidebar ttr-pinned-sidebar">
        <jsp:include page="../Common/heading/heading.jsp"/>
        <jsp:include page="../Common/slideBar/left.jsp" />

        <main class="ttr-wrapper">
            <div class="db-breadcrumb" style="display: flex; justify-content: center;">
                <h4 >Mapping Management</h4>
            </div>

            <div class="mb-4 ml-5">
                <form action="${pageContext.request.contextPath}/FLM/mapping" method="get">                                      
                    <div class="input-group input-group-sm">
                        <table class="auto-style1 mb-3">

                            <tbody>
                                <tr>
                                    <th class="auto-style13" style="text-align: right">Search by Curriculum Code                                     
                                    </th>                  
                                    <td class="auto-style10">
                                        <div class="form-inline">
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <input  type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Enter Curriculum ID..." name="cid" style=width:500px;>
                                                </span>                                                
                                                <div class="input-group-append">
                                                    <button type="submit" class="btn btn-secondary btn-number" style="background-color: orange; border-color: orange;">
                                                        <i class="fa fa-search" ></i>
                                                    </button>       
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </form>


                <!-- Đây là phần gửi method để edit -->
                <!--<form action="${pageContext.request.contextPath}/FLM/mapping" method="post">-->                   
                <table class="table table-bordered table-striped table-hover" ${hide ? 'style="display:none;"   ' : ''}>
                    <thead class="table-warning">
                        <c:choose>
                            <c:when test="${mapping.isEmpty()||mapping==null}">
                            <h5>${message}</h5>
                        </c:when>
                        <c:otherwise>
                            <tr>
                                <th>PLO(s)</th>
                                    <c:forEach items="${polist}" var="a">
                                    <th> <span title="${a.po.poDecription}">${a.po.poName}</span> </th>
                                    </c:forEach>
                            </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${plolist}" var="b">
                                    <tr>
                                        <td> <span title="${b.plo.plodescription}">${b.plo.ploname}</span></td>
                                            <c:forEach items="${polist}" var="a">
                                                <c:set var="checked" value="false" />
                                                <c:forEach items="${mapping}" var="p">
                                                    <c:if test="${p.plo.ploid == b.plo.ploid && p.po.poId == a.po.poId}">
                                                        <c:set var="checked" value="true" />
                                                    </c:if>
                                                </c:forEach>
                            <form action="${pageContext.request.contextPath}/FLM/mapping" method="post" id="updateMap${b.plo.ploid}_${a.po.poId}">
                                        <td>
                                            <input form="updateMap${b.plo.ploid}_${a.po.poId}" type="text" name="cru" hidden value="${cuId}"/>
                                            <input form="updateMap${b.plo.ploid}_${a.po.poId}" type="text" name="plo" hidden value="${b.plo.ploid}"/>
                                            <input form="updateMap${b.plo.ploid}_${a.po.poId}" type="text" name="po" hidden value="${a.po.poId}"/>
                                            <input form="updateMap${b.plo.ploid}_${a.po.poId}" type="text" name="check" hidden value="${checked}"/>
                                            <input onclick="submitForm('updateMap${b.plo.ploid}_${a.po.poId}')" form="updateMap${b.plo.ploid}_${a.po.poId}" type="checkbox" name="mapping" value="${checked}" <c:if test="${checked}">checked</c:if>> </td>
                                        </form>
                                </c:forEach>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                        <!--                            <div class="col-12">
                                                        <button type="submit" class="btn btn-secondary btn-number" style="background-color: orange; border-color: orange;">
                                                            Save change
                                                        </button>
                                                    </div>-->
                    </c:otherwise>
                </c:choose>

                <!--</form>-->


            </div>

        </main>

    </body>
    <script>
        function submitForm(formId) {
            document.getElementById(formId).submit();
        }
    </script>
    <script src="../view/profile/script.js"></script> 
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
    crossorigin="anonymous"></script>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct"
        crossorigin="anonymous"
    ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>


    <script src="../assets/vendors/bootstrap/js/popper.min.js"></script>
    <script src="../assets/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="../assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
    <script src="../assets/vendors/magnific-popup/magnific-popup.js"></script>
    <script src="../assets/vendors/counter/waypoints-min.js"></script>
    <script src="../assets/vendors/counter/counterup.min.js"></script>
    <script src="../assets/vendors/imagesloaded/imagesloaded.js"></script>
    <script src="../assets/vendors/masonry/masonry.js"></script>
    <script src="../assets/vendors/masonry/filter.js"></script>
    <script src="../assets/vendors/owl-carousel/owl.carousel.js"></script>
    <script src='../assets/vendors/scroll/scrollbar.min.js'></script>
    <script src="../assets/js/functions.js"></script>
    <script src="../assets/vendors/chart/chart.min.js"></script>
    <script src="../assets/js/admin.js"></script>
    <script src='../assets/vendors/switcher/switcher.js'></script>
</html>

