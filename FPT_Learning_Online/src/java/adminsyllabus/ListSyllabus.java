/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package adminsyllabus;

import dal.SyllabusDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Syllabus;
import sort.SortPage;

/**
 *
 * @author asus
 */
@WebServlet(name="ListSyllabus", urlPatterns={"/FLM/syllabuslist1"})
public class ListSyllabus extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListSyllabus</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListSyllabus at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String txt="";
        if (request.getParameter(txt)!=null) {
            txt = request.getParameter("txt");
        }          
       SyllabusDAO db = new   SyllabusDAO();
        String sort = request.getParameter("sort");
        String xpage = request.getParameter("page");
        if (sort == null) {
            sort = "idasc";
        }
        if (sort.equals("idasc")) {

            List< Syllabus> list = db.searchByCodeSyllabus1(txt);
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp=new SortPage();
            int[] arr=sp.getStartEnd(list.size(), xpage);
            List< Syllabus> listByPage = db.getListByPage(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);

        } else if (sort.equals("iddesc")) {
            List<Syllabus> list = db.searchByCodeSyllabus2(txt);
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp=new SortPage();
            int[] arr=sp.getStartEnd(list.size(), xpage);
            List<Syllabus> listByPage = db.getListByPage(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);
        }else if (sort.equals("codedesc")) {
            List<Syllabus> list = db.searchByCodeSyllabus3(txt);
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp=new SortPage();
            int[] arr=sp.getStartEnd(list.size(), xpage);
            List<Syllabus> listByPage = db.getListByPage(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);
        }else if (sort.equals("codeasc")) {
            List<Syllabus> list = db.searchByCodeSyllabus4(txt);
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp=new SortPage();
            int[] arr=sp.getStartEnd(list.size(), xpage);
            List<Syllabus> listByPage = db.getListByPage(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);
        }else if (sort.equals("typedesc")) {
            List<Syllabus> list = db.searchByCodeSyllabus5(txt);
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp=new SortPage();
            int[] arr=sp.getStartEnd(list.size(), xpage);
            List<Syllabus> listByPage = db.getListByPage(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);
        }else if (sort.equals("typeasc")) {
            List<Syllabus> list = db.searchByCodeSyllabus6(txt);
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp=new SortPage();
            int[] arr=sp.getStartEnd(list.size(), xpage);
            List<Syllabus> listByPage = db.getListByPage(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);
        } 
        request.setAttribute("sort", sort);
//       request.setAttribute("list2", list2);
         request.getRequestDispatcher("/view/admin/syllabus/list.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       String txt = request.getParameter("txt");
        SyllabusDAO syl = new SyllabusDAO();
        List<Syllabus> list = syl.searchByCodeSyllabus1(txt);

        if (list.isEmpty()) {
            String message = "Question " + txt + " does not exist!";
            request.setAttribute("message", message);
        }

        request.setAttribute("txt", txt);
        request.setAttribute("list", list);
 request.getRequestDispatcher("/view/admin/syllabus/list.jsp").forward(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
