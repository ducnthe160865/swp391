/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package adminsyllabus;

import dal.SubjectDBContext;
import dal.SyllabusDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Subject;

/**
 *
 * @author asus
 */
@WebServlet(name = "AddSyllabus", urlPatterns = {"/FLM/syllabusAdd"})
public class AddSyllabus extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddSyllabus</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddSyllabus at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("/view/admin/syllabus/add.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String subjectCode = request.getParameter("subjectCode");
        String syllabusNameEn = request.getParameter("syllabusNameEn");
        String noCredit = request.getParameter("noCredit");
        String degreeLevel = request.getParameter("degreeLevel");
        String description = request.getParameter("description");
        String decisionNo = request.getParameter("decisionNo");
        String approved = request.getParameter("isApproved");
        String active = request.getParameter("isActive");
//
//         String[] subjects = request.getParameterValues("subjects");


        SyllabusDAO dao = new SyllabusDAO();
         SubjectDBContext subjectDB = new SubjectDBContext();
//         ArrayList<Subject> subjects = subjectDB.subjectSelect();
        try {
            boolean isActive = Boolean.parseBoolean(active);
            boolean isApproved = Boolean.parseBoolean(approved);
            dao.addSyllabus(syllabusNameEn, subjectCode, noCredit, degreeLevel, description, decisionNo, isApproved, isActive);
        } catch (Exception e) {
        }
        HttpSession session = request.getSession();
        session.setAttribute("message", "Update");
//        request.setAttribute("subjects", subjects);
        response.sendRedirect("syllabuslist1");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
