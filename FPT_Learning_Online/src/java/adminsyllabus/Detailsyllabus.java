/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package adminsyllabus;

import dal.SyllabusDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Syllabus;

/**
 *
 * @author asus
 */
@WebServlet(name = "Detailsyllabus", urlPatterns = {"/FLM/detailsyllabus"})
public class Detailsyllabus extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Detailsyllabus</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Detailsyllabus at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_raw = request.getParameter("syllabusId");
        int id = 0;
        try {
            id = Integer.parseInt(id_raw);

        } catch (NumberFormatException e) {
            System.out.println("Questiondetail - get " + e);
        }
        SyllabusDAO dao = new SyllabusDAO();
        Syllabus decision = dao.getSyllabusBySyllabusID(id);
        request.setAttribute("decision", decision);
        request.getRequestDispatcher("/view/admin/syllabus/detailsyl.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        SyllabusDAO dao = new SyllabusDAO();

        String syllabusId_raw = request.getParameter("syllabusId");
        String subjectCode = request.getParameter("subjectCode");
        String syllabusNameEn = request.getParameter("syllabusNameEn");

        String active = request.getParameter("isActive");
        String approved = request.getParameter("isApproved");
        boolean isActive = active.equalsIgnoreCase("true");
        boolean isApproved = approved.equalsIgnoreCase("true");
        String decisionNo = request.getParameter("decisionNo");
        String degreeLevel = request.getParameter("degreeLevel");
        String description = request.getParameter("description");

        String sid_raw = request.getParameter("noCredit");
        
        int qid, sid;
        //textarea
        response.getWriter().write(description);
        try {
            qid = Integer.parseInt(syllabusId_raw);
            sid = Integer.parseInt(sid_raw);

            Syllabus q = dao.getSyllabusBySyllabusID(qid);
            
            q.setSubjectCode(subjectCode);
            q.setSyllabusNameEn(syllabusNameEn);
            q.setNoCredit(sid);
            q.setIsActive(isActive);
            q.setIsApproved(isApproved);
            q.setDegreeLevel(degreeLevel);
            q.setDescription(description);
            dao.editSyllabus(q);
        } catch (NumberFormatException e) {
            System.out.println("edit" + e);
        }
        
        HttpSession session1 = request.getSession();
        session1.setAttribute("message", "Update");
        
        response.sendRedirect("detailsyllabus?id" + syllabusId_raw);
        
        
        request.getRequestDispatcher("/view/admin/syllabus/detailsyl.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
