/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sort;

/**
 *
 * @author bongt
 */
public class SortPage {
    public int[] getStartEnd(int listsize,String xpage){
        int[] arr= new int[4];
        int page, numberPerPage = 4;
            int size;
            
                size = listsize;
            
            int numberOfPage = ((size % numberPerPage == 0) ? (size / numberPerPage) : (size / numberPerPage + 1));
            if (xpage == null) {
                page = 1;
            } else {
                page = Integer.parseInt(xpage);
            }
            int start, end;
            start = (page - 1) * numberPerPage;
            end = Math.min(page * numberPerPage, size);
            arr[0]=start;
            arr[1]=end;
            arr[2]=page;
            arr[3]=numberOfPage;
        return arr;
    }
}
