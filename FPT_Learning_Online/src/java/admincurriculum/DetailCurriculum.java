/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package admincurriculum;

import dal.CurriculumDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Curriculum;

/**
 *
 * @author asus
 */
@WebServlet(name="DetailCurriculum", urlPatterns={"/FLM/detailcurriculum"})
public class DetailCurriculum extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DetailCurriculum</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DetailCurriculum at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
         String id_raw = request.getParameter("curriculumId");
        int id = 0;
        try {
            id = Integer.parseInt(id_raw);

        } catch (NumberFormatException e) {
            System.out.println("Questiondetail - get " + e);
        }
        CurriculumDAO dao = new CurriculumDAO();
        Curriculum decision = dao.getCurriculumByCurriculumID(id);
        request.setAttribute("decision", decision);
        request.getRequestDispatcher("/view/admin/curriculum/detailcur.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        CurriculumDAO dao = new CurriculumDAO();
        String curriculumId_raw = request.getParameter("curriculumId");
        
        String curriculumCode = request.getParameter("curriculumCode");
        String nameEn = request.getParameter("nameEn");
        String decisionNo = request.getParameter("decisionNo");
        String totalCredit = request.getParameter("totalCredit");
        String active = request.getParameter("isActive");
        boolean isActive = active.equalsIgnoreCase("true");
        
        String decription = request.getParameter("decription");
int qid;
        //textarea
        response.getWriter().write(decription);
        try {
            qid = Integer.parseInt(curriculumId_raw);
        

            Curriculum q = dao.getCurriculumByCurriculumID(qid);
            
            q.setCurriculumCode(curriculumCode);
            q.setNameEn(nameEn);
            q.setIsActive(isActive);
            q.setDecription(decription);
            
            dao.editCurriculum(q);
        } catch (NumberFormatException e) {
            System.out.println("edit" + e);
        }
        
        HttpSession session1 = request.getSession();
        session1.setAttribute("message", "Update");       
        response.sendRedirect("detailcurriculum?id" + curriculumId_raw);                         
        request.getRequestDispatcher("/view/admin/curriculum/detailcur.jsp").forward(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
