/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.List;

/**
 *
 * @author bongt
 */
public class MapSubPlo {
    private String subcode;
    private List<String> plo;

    public MapSubPlo(String subcode, List<String> plo) {
        this.subcode = subcode;
        this.plo = plo;
    }

    public MapSubPlo() {
    }

    public String getSubcode() {
        return subcode;
    }

    public void setSubcode(String subcode) {
        this.subcode = subcode;
    }

    public List<String> getPlo() {
        return plo;
    }

    public void setPlo(List<String> plo) {
        this.plo = plo;
    }
    
    
}
