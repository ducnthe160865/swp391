/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package questionController;

import dal.QuestionDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Question1;
import model.Session;
import model.Syllabus;
import sort.SortPage;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "ShowQuestionServletController", urlPatterns = {"/FLM/showquestion"})
public class ShowQuestionServletController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ShowQuestionServletController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ShowQuestionServletController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String txt = "";
        QuestionDAO sDao = new QuestionDAO();
        String syllabusID_raw = request.getParameter("syllabusId");
    
        if (request.getParameter(txt) != null) {
            txt = request.getParameter("txt");
        }
        QuestionDAO db = new QuestionDAO();
        String sort = request.getParameter("sort");
        String xpage = request.getParameter("page");
        if (sort == null) {
            sort = "idasc";
        }
        if (sort.equals("idasc")) {

//            List< Question1> list = db.searchByCodeQuestion(txt);
            List<Question1> list = sDao.getQuestionBySyllabusID(Integer.parseInt(syllabusID_raw));
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp = new SortPage();
            int[] arr = sp.getStartEnd(list.size(), xpage);
            List< Question1> listByPage = db.getListByPage1(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);

        } else if (sort.equals("iddesc")) {
            List<Question1> list = db.searchByCodeQuestion1(txt);
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp = new SortPage();
            int[] arr = sp.getStartEnd(list.size(), xpage);
            List<Question1> listByPage = db.getListByPage1(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);
        } else if (sort.equals("codedesc")) {
            List<Question1> list = db.searchByCodeQuestion2(txt);
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp = new SortPage();
            int[] arr = sp.getStartEnd(list.size(), xpage);
            List<Question1> listByPage = db.getListByPage1(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);
        } else if (sort.equals("codeasc")) {
            List<Question1> list = db.searchByCodeQuestion3(txt);
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp = new SortPage();
            int[] arr = sp.getStartEnd(list.size(), xpage);
            List<Question1> listByPage = db.getListByPage1(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);
        } else if (sort.equals("typedesc")) {
            List<Question1> list = db.searchByCodeQuestion4(txt);
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp = new SortPage();
            int[] arr = sp.getStartEnd(list.size(), xpage);
            List<Question1> listByPage = db.getListByPage1(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);
        } else if (sort.equals("typeasc")) {
            List<Question1> list = db.searchByCodeQuestion5(txt);
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp = new SortPage();
            int[] arr = sp.getStartEnd(list.size(), xpage);
            List<Question1> listByPage = db.getListByPage1(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);
        }
        request.setAttribute("sort", sort);
        request.setAttribute("syllabusId", syllabusID_raw);
//       request.setAttribute("list2", list2);
        request.getRequestDispatcher("/view/admin/question/showquestion.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String txt = request.getParameter("txt");
        QuestionDAO syl = new QuestionDAO();
        List<Question1> list = syl.searchByCodeQuestion(txt);
//        List<Sub> list2 = syl.searchByNameSyl(txt);

        if (list.isEmpty()) {
            String message = "Question " + txt + " does not exist!";
            request.setAttribute("message", message);
        }

        request.setAttribute("txt", txt);
        request.setAttribute("list", list);
        request.getRequestDispatcher("/view/admin/question/showquestion.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
