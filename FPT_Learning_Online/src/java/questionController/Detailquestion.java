/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package questionController;

import dal.DecisionDAO;
import dal.QuestionDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Question;
import model.Question1;

/**
 *
 * @author asus
 */
@WebServlet(name = "Detailquestion", urlPatterns = {"/FLM/detailquestion"})
public class Detailquestion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Detailquestion</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Detailquestion at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_raw = request.getParameter("questionId");
        int id = 0;
        try {
            id = Integer.parseInt(id_raw);

        } catch (NumberFormatException e) {
            System.out.println("Questiondetail - get " + e);
        }
        QuestionDAO dao = new QuestionDAO();
        Question1 decision = dao.getQuestionByQuestionID(id);
        request.setAttribute("decision", decision);
        request.getRequestDispatcher("/view/admin/question/detail.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String qid_raw = request.getParameter("qid");
        String sid_raw = request.getParameter("sid");
          String session_raw = request.getParameter("session");
          
        QuestionDAO dao = new QuestionDAO();
        String name = request.getParameter("name");
        String detail = request.getParameter("detail");
//       
        int qid, sid,session;
        response.getWriter().write(detail);
        try {
            qid = Integer.parseInt(qid_raw);
            sid = Integer.parseInt(sid_raw);
             session = Integer.parseInt(sid_raw);
            Question1 q = dao.getQuestionByQuestionID(qid);
            q.setName(name);
            q.setDetail(detail);
            dao.editQuestion(q);
        } catch (NumberFormatException e) {
            System.out.println("edit" + e);
        }
        HttpSession session1 = request.getSession();
        session1.setAttribute("message", "Update");
        response.sendRedirect("detailquestion?id" + sid_raw);
        
        
        request.getRequestDispatcher("/view/admin/question/detail.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
