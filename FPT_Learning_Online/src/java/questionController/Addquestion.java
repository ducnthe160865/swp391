/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package questionController;

import dal.QuestionDAO;
import dal.SyllabusDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Syllabus;

/**
 *
 * @author asus
 */
@WebServlet(name="Addquestion", urlPatterns={"/FLM/questionAdd"})
public class Addquestion extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Addquestion</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Addquestion at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
          String sid_raw = request.getParameter("syllabusId");
        request.setAttribute("sid", sid_raw);
        SyllabusDBContext dao = new SyllabusDBContext();
        try {
            int sid= Integer.parseInt(sid_raw);
            Syllabus s=dao.getSyllabusByID(sid);
            request.setAttribute("sy", s);
            System.out.println(s.getSyllabusNameEn());
        } catch (Exception e) {
        }
                
 request.getRequestDispatcher("/view/admin/question/add.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
            String sid_raw = request.getParameter("sid");  
        String name = request.getParameter("name");
        String detail = request.getParameter("detail");
        
        QuestionDAO dao = new QuestionDAO();
        System.out.println(sid_raw);
  try {
            int sid = Integer.parseInt(sid_raw);
            dao.insertQuestion(sid, name, detail);
        } catch (Exception e) {
        }
  HttpSession session = request.getSession();
        session.setAttribute("message", "Update");
        response.sendRedirect("showquestion?syllabusId=" + sid_raw);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
