/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Subject;
import model.Syllabus;

/**
 *
 * @author asus
 */
public class SyllabusDAO extends DBContext<Syllabus> {
    public ArrayList<Subject> subjectSelect() {
        try {
            String sql = "SELECT subjectCode FROM subject";
            PreparedStatement stm = connection.prepareStatement(sql);
            ArrayList<Subject> subjects = new ArrayList<>();
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Subject s = new Subject();
                s.setSubjectCode(rs.getString("subjectCode"));
                subjects.add(s);
            }
            return subjects;
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public void addSyllabus(String syllabusNameEn, String subjectCode, String noCredit, String degreeLevel, String description, String decisionNo, boolean approved, boolean active) {
        String sql = "INSERT INTO `flm`.`syllabus`\n"
                + "(\n"
                + "`syllabusNameEn`,\n"
                + "`subjectCode`,\n"
                + "`noCredit`,\n"
                + "`degreeLevel`,\n"
                + "`description`,\n"
                + "`decisionNo`,\n"
                + "`isApproved`,\n"
                + "`isActive`)\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?,?);";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, syllabusNameEn);
            st.setString(2, subjectCode);
            st.setString(3, noCredit);
            st.setString(4, degreeLevel);
            st.setString(5, description);
            st.setString(6, decisionNo);
            st.setBoolean(7, approved);
            st.setBoolean(8, active);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public List<Syllabus> listByPage2(List<Syllabus> selist, int start, int end) {
        List<Syllabus> list = new ArrayList<>();
        for (int i = start; i < end; i++) {
            list.add(selist.get(i));
        }
        return list;
    }
   public List<Syllabus> getListByPage(List<Syllabus> list, int start, int end) {
        List<Syllabus> result = new ArrayList<>();

        for (int i = start; i < end; i++) {
            result.add(list.get(i));
        }
        return result;
    }
   public List<Syllabus> searchByCodeSyllabus1(String txtSearch) {
        List<Syllabus> list = new ArrayList<>();
        String query = "SELECT a.syllabusId,a.subjectCode,a.syllabusNameEn,a.syllabusNameVn,a.isActive,a.isApproved,concat(a.decisionNo,' dated ',DATE_FORMAT(d.approvedDate,'%d/%m/%Y')),a.noCredit,a.degreeLevel,a.description\n"
                + "\n"
                + "FROM Syllabus a\n"
                + "\n"
                + "join Decision d on a.decisionNo=d.decisionNo \n"
                + "where a.subjectCode like '%" + txtSearch + "%' " + " or a.syllabusNameEn like '%" + txtSearch + "%'  order by syllabusId asc";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Syllabus(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getBoolean(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
   public List<Syllabus> searchByCodeSyllabus2(String txtSearch) {
        List<Syllabus> list = new ArrayList<>();
        String query = "SELECT a.syllabusId,a.subjectCode,a.syllabusNameEn,a.syllabusNameVn,a.isActive,a.isApproved,concat(a.decisionNo,' dated ',DATE_FORMAT(d.approvedDate,'%d/%m/%Y')),a.noCredit,a.degreeLevel,a.description\n"
                + "\n"
                + "FROM Syllabus a\n"
                + "\n"
                + "join Decision d on a.decisionNo=d.decisionNo \n"
                + "where a.subjectCode like '%" + txtSearch + "%' " + " or a.syllabusNameEn like '%" + txtSearch + "%'  order by syllabusId desc";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Syllabus(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getBoolean(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
      public List<Syllabus> searchByCodeSyllabus4(String txtSearch) {
        List<Syllabus> list = new ArrayList<>();
        String query = "SELECT a.syllabusId,a.subjectCode,a.syllabusNameEn,a.syllabusNameVn,a.isActive,a.isApproved,concat(a.decisionNo,' dated ',DATE_FORMAT(d.approvedDate,'%d/%m/%Y')),a.noCredit,a.degreeLevel,a.description\n"
                + "\n"
                + "FROM Syllabus a\n"
                + "\n"
                + "join Decision d on a.decisionNo=d.decisionNo \n"
                + "where a.subjectCode like '%" + txtSearch + "%' " + " or a.syllabusNameEn like '%" + txtSearch + "%'  order by subjectCode desc";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Syllabus(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getBoolean(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
         public List<Syllabus> searchByCodeSyllabus3(String txtSearch) {
        List<Syllabus> list = new ArrayList<>();
        String query = "SELECT a.syllabusId,a.subjectCode,a.syllabusNameEn,a.syllabusNameVn,a.isActive,a.isApproved,concat(a.decisionNo,' dated ',DATE_FORMAT(d.approvedDate,'%d/%m/%Y')),a.noCredit,a.degreeLevel,a.description\n"
                + "\n"
                + "FROM Syllabus a\n"
                + "\n"
                + "join Decision d on a.decisionNo=d.decisionNo \n"
                + "where a.subjectCode like '%" + txtSearch + "%' " + " or a.syllabusNameEn like '%" + txtSearch + "%'  order by subjectCode asc";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Syllabus(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getBoolean(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
                  public List<Syllabus> searchByCodeSyllabus5(String txtSearch) {
        List<Syllabus> list = new ArrayList<>();
        String query = "SELECT a.syllabusId,a.subjectCode,a.syllabusNameEn,a.syllabusNameVn,a.isActive,a.isApproved,concat(a.decisionNo,' dated ',DATE_FORMAT(d.approvedDate,'%d/%m/%Y')),a.noCredit,a.degreeLevel,a.description\n"
                + "\n"
                + "FROM Syllabus a\n"
                + "\n"
                + "join Decision d on a.decisionNo=d.decisionNo \n"
                + "where a.subjectCode like '%" + txtSearch + "%' " + " or a.syllabusNameEn like '%" + txtSearch + "%'  order by syllabusNameEn asc";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Syllabus(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getBoolean(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
                           public List<Syllabus> searchByCodeSyllabus6(String txtSearch) {
        List<Syllabus> list = new ArrayList<>();
        String query = "SELECT a.syllabusId,a.subjectCode,a.syllabusNameEn,a.syllabusNameVn,a.isActive,a.isApproved,concat(a.decisionNo,' dated ',DATE_FORMAT(d.approvedDate,'%d/%m/%Y')),a.noCredit,a.degreeLevel,a.description\n"
                + "\n"
                + "FROM Syllabus a\n"
                + "\n"
                + "join Decision d on a.decisionNo=d.decisionNo \n"
                + "where a.subjectCode like '%" + txtSearch + "%' " + " or a.syllabusNameEn like '%" + txtSearch + "%'  order by syllabusNameEn desc";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Syllabus(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getBoolean(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
    public List<Syllabus> getAll() {
        List<Syllabus> list = new ArrayList<>();

        try {
            String sql = "SELECT `syllabus`.`syllabusId`,\n"
                    + "    `syllabus`.`subjectCode`,\n"
                    + "    `syllabus`.`syllabusNameEn`,\n"
                    + "    `syllabus`.`syllabusNameVn`,\n"
                    + "    `syllabus`.`isApproved`,\n"
                    + "    `syllabus`.`isActive`,\n"
                    + "    `syllabus`.`decisionNo`,\n"
                    + "    `syllabus`.`noCredit`,\n"
                    + "    `syllabus`.`degreeLevel`,\n"
                    + "    `syllabus`.`description`\n"
                    + "FROM `flm`.`syllabus`;";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Syllabus decision = new Syllabus(rs.getInt("syllabusId"), rs.getString("subjectCode"), rs.getString("syllabusNameEn"), rs.getString("syllabusNameVn"), rs.getBoolean("isApproved"), rs.getBoolean("isActive"), rs.getString("decisionNo"), rs.getInt("noCredit"), rs.getString("degreeLevel"), rs.getString("description"));

                list.add(decision);
            }
        } catch (SQLException e) {
            System.out.println("decisionDAO, getDecisionByDecisionID" + e);
        }
        return list;
    }

    public Syllabus getSyllabusBySyllabusID(int syllabusId) {
        try {
            String sql = "SELECT `syllabus`.`syllabusId`,\n"
                    + "    `syllabus`.`subjectCode`,\n"
                    + "    `syllabus`.`syllabusNameEn`,\n"
                    + "    `syllabus`.`syllabusNameVn`,\n"
                    + "    `syllabus`.`isApproved`,\n"
                    + "    `syllabus`.`isActive`,\n"
                    + "    `syllabus`.`decisionNo`,\n"
                    + "    `syllabus`.`noCredit`,\n"
                    + "    `syllabus`.`degreeLevel`,\n"
                    + "    `syllabus`.`description`\n"
                    + "FROM `flm`.`syllabus`"
                    + "WHERE `syllabus`.`syllabusId` = ?;";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, syllabusId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Syllabus decision = new Syllabus(rs.getInt("syllabusId"), rs.getString("subjectCode"), rs.getString("syllabusNameEn"), rs.getString("syllabusNameVn"), rs.getBoolean("isApproved"), rs.getBoolean("isActive"), rs.getString("decisionNo"), rs.getInt("noCredit"), rs.getString("degreeLevel"), rs.getString("description"));
                return decision;
            }
        } catch (SQLException e) {
            System.out.println("decisionDAO, getDecisionByDecisionID" + e);
        }
        return null;
    }

    public void editSyllabus(Syllabus q) {
        String sql = "UPDATE `flm`.`syllabus`\n"
                + "SET\n"
                + "`syllabusNameEn` = ?,\n"
                + "`syllabusNameVn` = ?,\n"
                + "`subjectCode` =?, \n"
                + "`noCredit` = ?,\n"
                + "`degreeLevel` = ?,\n"
                + "`description` = ?,\n"
                + "`isApproved` = ?,\n"
                + "`isActive` = ?\n"
                + "WHERE `syllabusId` = ?;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, q.getSyllabusNameEn());
            st.setString(2, q.getSyllabusNameEn());
            st.setString(3, q.getSubjectCode());
            st.setInt(4, q.getNoCredit());
            st.setString(5, q.getDegreeLevel());
            st.setString(6, q.getDescription());
            st.setBoolean(7, q.isIsActive());
            st.setBoolean(8, q.isIsApproved());
            st.setInt(9, q.getSyllabusId());

            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    @Override
    public void insert(Syllabus model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void update(Syllabus model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void delete(Syllabus model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Syllabus getStringId(String id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Syllabus get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArrayList<Syllabus> list() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
