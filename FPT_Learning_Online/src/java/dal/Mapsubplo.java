/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import com.oracle.wls.shaded.org.apache.bcel.generic.AALOAD;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.MapSubPlo;

/**
 *
 * @author bongt
 */
public class Mapsubplo extends DBContext<Object> {

    public List<MapSubPlo> selectMapSubPlo(String txt) {
        List<MapSubPlo> list=new ArrayList<>();
        
        String query = "select ps.subjectCode\n"
                + "from curriculum_subject sc join plo_subject ps on sc.subjectCode=ps.subjectcode \n"
                + "where sc.curriculumId=? GROUP BY ps.subjectCode;";
        String query2 = "select ploName\n"
                + "from  plo_subject ps  join plo on ps.ploid=plo.ploId\n"
                + "where ps.subjectcode=?";

        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, txt);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                List<String> list2= new ArrayList<>();
                String subcode=rs.getString(1);
//                System.out.println(subcode);
                try {
                    PreparedStatement st2 = connection.prepareStatement(query2);
                    st2.setString(1, subcode);
                    ResultSet rs2 = st2.executeQuery();
                    while (rs2.next()) {
                        list2.add(rs2.getString(1));
                    }
                } catch (Exception e) {
                    System.out.println("loi get plo mapping"+e.getMessage());
                }
                list.add( new MapSubPlo(subcode, list2));
            }
        } catch (Exception e) {
            System.out.println("loi get subcode ben mapping"+e.getMessage());
        }

        return list;
    }

    @Override
    public void insert(Object model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void update(Object model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void delete(Object model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object getStringId(String id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArrayList<Object> list() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
