/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Curriculum;
import model.Mapping;
import model.PLO;
import model.PO;

/**
 *
 * @author l
 */
public class PoDBContext extends DBContext<PO> {

     
    public List<Curriculum> getCurriCode() {
        List<Curriculum> list = new ArrayList<>();
        String sql = "SELECT curriculumCode FROM flm.curriculum;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Curriculum(
                        rs.getString("curriculumCode")));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public PLO getPlobyID(int pid) {
        try {
            String sql = "SELECT plo.ploId,plo.ploName,ploDecription,curriculum.curriculumCode\n"
                    + "FROM plo\n"
                    + "Inner join curriculum On curriculum.curriculumId = plo.curriculumId\n"
                    + "where `plo`.`ploId` = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, pid);
            ResultSet rs = stm.executeQuery();
            PLO p = new PLO();
            while (rs.next()) {
                PLO plo = new PLO();
                plo.setPloid(rs.getInt("ploId"));
                plo.setPloname(rs.getString("ploName"));
                plo.setPlodescription(rs.getString("ploDecription"));

                Curriculum c = new Curriculum();
                c.setCurriculumCode(rs.getString("curriculumCode"));
                plo.setCurriculum(c);
                p = plo;
            }
            return p;
        } catch (SQLException ex) {
            Logger.getLogger(CurriculumDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public PO getPobyID(int pid) {
        try {
            String sql = "SELECT po.poId,po.poName,poDecription,curriculum.curriculumCode\n"
                    + "FROM po\n"
                    + "Inner join curriculum On curriculum.curriculumId = po.curriculumId\n"
                    + "where `po`.`poId` = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, pid);
            ResultSet rs = stm.executeQuery();
            PO p = new PO();
            while (rs.next()) {
                PO po = new PO();
                po.setPoId(rs.getInt("poId"));
                po.setPoName(rs.getString("poName"));
                po.setPoDecription(rs.getString("poDecription"));

                Curriculum c = new Curriculum();
                c.setCurriculumCode(rs.getString("curriculumCode"));
                po.setCurriculum(c);
                p = po;
            }
            return p;
        } catch (SQLException ex) {
            Logger.getLogger(PoDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Integer> getPOLargestID() {
        List<Integer> list = new ArrayList<>();
        String sql = "SELECT MAX(poid) FROM flm.po";
        try (
                 PreparedStatement ps = connection.prepareStatement(sql);  ResultSet rs = ps.executeQuery();) {
            if (rs.next()) {
                list.add(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Integer> getPLOLargestID() {
        List<Integer> list = new ArrayList<>();
        String sql = "SELECT MAX(ploId) FROM flm.plo";
        try (
                 PreparedStatement ps = connection.prepareStatement(sql);  ResultSet rs = ps.executeQuery();) {
            if (rs.next()) {
                list.add(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<Curriculum> getPOPLODetail(int cid) {
        ArrayList<Curriculum> curriculumPOList = new ArrayList<>();
        try {
            String sql = "SELECT curriculum.curriculumid,curriculum.curriculumCode, curriculum.nameEn,\n"
                    + "po.poId,po.poName,po.poDecription,\n"
                    + "plo.ploId,plo.ploName,plo.ploDecription\n"
                    + "FROM curriculum\n"
                    + "LEFT JOIN po ON po.curriculumId = curriculum.curriculumId\n"
                    + "Left join plo on plo.curriculumId = curriculum.curriculumId\n"
                    + "where curriculum.curriculumId = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, cid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Curriculum c = new Curriculum();
                c.setCurriculumId(rs.getInt("curriculumid"));
                c.setCurriculumCode(rs.getString("curriculumCode"));
                c.setNameEn(rs.getString("nameEn"));

                PO po = new PO();
                po.setPoId(rs.getInt("poId"));
                po.setPoName(rs.getString("poName"));
                po.setPoDecription(rs.getString("poDecription"));
                c.setPo(po);

                PLO plo = new PLO();
                plo.setPloid(rs.getInt("ploId"));
                plo.setPloname(rs.getString("ploName"));
                plo.setPlodescription(rs.getString("ploDecription"));
                c.setPlo(plo);

                curriculumPOList.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CurriculumDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return curriculumPOList;
    }

    public ArrayList<Curriculum> getPODetail(int cid) {
        ArrayList<Curriculum> curriculumPOList = new ArrayList<>();
        try {
            String sql = "SELECT curriculum.curriculumid,curriculum.curriculumCode, curriculum.nameEn,\n"
                    + "po.poId,po.poName,po.poDecription\n"
                    + "FROM `flm`.`curriculum`\n"
                    + "LEFT JOIN PO ON po.curriculumId = curriculum.curriculumId\n"
                    + "where curriculum.curriculumId = ?;;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, cid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Curriculum c = new Curriculum();
                c.setCurriculumId(rs.getInt("curriculumid"));
                c.setCurriculumCode(rs.getString("curriculumCode"));
                c.setNameEn(rs.getString("nameEn"));

                PO po = new PO();
                po.setPoId(rs.getInt("poId"));
                po.setPoName(rs.getString("poName"));
                po.setPoDecription(rs.getString("poDecription"));
                c.setPo(po);

                curriculumPOList.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CurriculumDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return curriculumPOList;
    }

    public ArrayList<Curriculum> getPLODetail(int cid) {
        ArrayList<Curriculum> curriculumPLOList = new ArrayList<>();
        try {
            String sql = "SELECT curriculum.curriculumid, curriculum.curriculumCode, curriculum.nameEn,\n"
                    + "plo.ploid, plo.ploName, plo.ploDecription\n"
                    + "FROM `flm`.`curriculum`\n"
                    + "LEFT JOIN plo ON plo.curriculumId = curriculum.curriculumId\n"
                    + "where curriculum.curriculumId = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, cid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Curriculum c = new Curriculum();
                c.setCurriculumId(rs.getInt("curriculumid"));
                c.setCurriculumCode(rs.getString("curriculumCode"));
                c.setNameEn(rs.getString("nameEn"));

                PLO plo = new PLO();
                plo.setPloid(rs.getInt("ploid"));
                plo.setPloname(rs.getString("ploName"));
                plo.setPlodescription(rs.getString("ploDecription"));
                c.setPlo(plo);

                curriculumPLOList.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CurriculumDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return curriculumPLOList;
    }

    public ArrayList<Curriculum> getPLO(int cid) {
        ArrayList<Curriculum> curriculumPLOList = new ArrayList<>();
        try {
            String sql = "SELECT curriculum.curriculumCode, curriculum.nameEn,\n"
                    + "plo.ploid, plo.ploName, plo.ploDecription\n"
                    + "FROM `flm`.`curriculum`\n"
                    + "LEFT JOIN plo ON plo.curriculumId = curriculum.curriculumId\n"
                    + "where curriculum.curriculumId = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, cid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Curriculum c = new Curriculum();
                c.setCurriculumCode(rs.getString("curriculumCode"));
                c.setNameEn(rs.getString("nameEn"));

                PLO plo = new PLO();
                plo.setPloid(rs.getInt("ploid"));
                plo.setPloname(rs.getString("ploName"));
                plo.setPlodescription(rs.getString("ploDecription"));
                c.setPlo(plo);

                curriculumPLOList.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CurriculumDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return curriculumPLOList;
    }

    public List<PLO> searchPLObyCode(String ploid) {
        List<PLO> listplo = new ArrayList<>();
        String sql = "select plo.ploId,plo.ploName,ploDecription,curriculum.curriculumCode\n"
                + "from plo\n"
                + "Inner join curriculum On curriculum.curriculumId = plo.curriculumId\n"
                + "where curriculum.curriculumCode like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + ploid + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                PLO plo = new PLO();
                plo.setPloid(rs.getInt("ploId"));
                plo.setPloname(rs.getString("ploName"));
                plo.setPlodescription(rs.getString("ploDecription"));

                Curriculum c = new Curriculum();
                c.setCurriculumCode(rs.getString("curriculumCode"));
                plo.setCurriculum(c);
                listplo.add(plo);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return listplo;
    }

    public ArrayList<PO> SearchPObyCode(String poid) {
        ArrayList<PO> listpo = new ArrayList<>();
        try {
            String sql = "select po.poId,po.poName,poDecription,curriculum.curriculumCode\n"
                    + "from po\n"
                    + "Inner join curriculum On curriculum.curriculumId = po.curriculumId\n"
                    + "where curriculum.curriculumCode like ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + poid + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                PO po = new PO();
                po.setPoId(rs.getInt("poId"));
                po.setPoName(rs.getString("poName"));
                po.setPoDecription(rs.getString("poDecription"));

                Curriculum c = new Curriculum();
                c.setCurriculumCode(rs.getString("curriculumCode"));
                po.setCurriculum(c);
                listpo.add(po);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CurriculumDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listpo;
    }

    public void InsertPo(int poId, String poName, String poDecription, String curriculumCode) {
        try {
            String sql = "INSERT INTO po (poId, curriculumId, poName, poDecription) "
                    + "VALUES (?, (SELECT curriculumId FROM curriculum WHERE curriculumCode = ?), ?, ?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, poId);
            st.setString(2, curriculumCode);
            st.setString(3, poName);
            st.setString(4, poDecription);
            st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void InsertPlo(int ploId, String ploName, String ploDecription, String curriculumCode) {
        try {
            String sql = "INSERT INTO plo (ploId, curriculumId, ploName, ploDecription) \n"
                    + "VALUES (?, (SELECT curriculumId FROM curriculum WHERE curriculumCode = ?), ?, ?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, ploId);
            st.setString(2, curriculumCode);
            st.setString(3, ploName);
            st.setString(4, ploDecription);
            st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updatePO(int poId, String curriculumCode, String poName, String poDecription) {
        try {
            String sql = "UPDATE po "
                    + "SET po.curriculumId = (SELECT curriculumId FROM curriculum WHERE curriculum.curriculumCode = ?), "
                    + "po.poName = ?, "
                    + "po.poDecription = ? "
                    + "WHERE po.poId = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, curriculumCode);
            stm.setString(2, poName);
            stm.setString(3, poDecription);
            stm.setInt(4, poId);
            stm.executeUpdate();
            System.out.println("PO updated successfully.");
        } catch (SQLException e) {
            System.out.println("Error updating PO: " + e.getMessage());
        }
    }

    public void UpdatePlo(int ploId, String curriculumCode, String ploName, String ploDecription) {
        try {
            String sql = "UPDATE plo \n"
                    + "SET\n"
                    + "plo.curriculumId = (SELECT curriculumId FROM curriculum WHERE curriculum.curriculumCode = ?), \n"
                    + "plo.ploName = ?, \n"
                    + "plo.ploDecription = ? \n"
                    + "WHERE plo.ploId = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, curriculumCode);
            stm.setString(2, ploName);
            stm.setString(3, ploDecription);
            stm.setInt(4, ploId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PoDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void insert(PO model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void update(PO model) {

    }

    @Override
    public void delete(PO model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public PO getStringId(String id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public PO get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArrayList<PO> list() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<PLO> getAllPLO() {
        List<PLO> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM flm.plo\n";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                PLO plo = new PLO();
                plo.setPloid(rs.getInt("ploId"));
                plo.setPloname(rs.getString("ploName"));
                plo.setPlodescription(rs.getString("ploDecription")); 
                list.add(plo);
            }

        } catch (SQLException ex) {
            Logger.getLogger(CurriculumDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public static void main(String[] args) {
        
    }
}
