/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Curriculum;
import model.Mapping;
import model.PLO;
import model.PO;

/**
 *
 * @author l
 */
public class MappingDBContext extends DBContext<Mapping> {

    public Mapping GetMappingByPO(int pid) {
        try {
            String sql = "SELECT * FROM flm.mapping where poid=?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, pid);
            ResultSet rs = stm.executeQuery();
            Mapping p = new Mapping();
            while (rs.next()) {
                p.setPoid(rs.getInt("poId"));
                p.setPloid(rs.getInt("ploId"));
                p.setCurriculumid(rs.getInt("curriculumId"));
            }
            return p;
        } catch (SQLException ex) {
            Logger.getLogger(CurriculumDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int GetIdCurriculumByPOCode(String pid) {
        try {
            String sql = "SELECT * FROM flm.curriculum where curriculumCode = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, pid);
            ResultSet rs = stm.executeQuery();
            Curriculum p = new Curriculum();
            if (rs.next()) {
                return rs.getInt("curriculumId");
            }
        } catch (SQLException ex) {
            Logger.getLogger(CurriculumDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public List<Mapping> getAllMappings() {
        List<Mapping> list = new ArrayList<>();
        String sql = "SELECT * FROM flm.mapping";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Mapping(rs.getInt("ploId"), rs.getInt("poId"), rs.getInt("curriculumId")));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public ArrayList<Mapping> getMapping(int cuid) {
        ArrayList<Mapping> mapping = new ArrayList<>();
        try {
            String sql = "SELECT plo.ploName,po.poName,plo.ploId,po.poId\n"
                    + "FROM mapping\n"
                    + "JOIN po ON mapping.poId = po.poId\n"
                    + "JOIN plo ON mapping.ploId = plo.ploId\n"
                    + "JOIN curriculum ON mapping.curriculumId = curriculum.curriculumId\n"
                    + "WHERE curriculum.curriculumId = ?\n"
                    + "Order by PO.poName";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, cuid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Mapping m = new Mapping();
                PO po = new PO();
                PLO plo = new PLO();
                Curriculum c = new Curriculum();
                po.setPoId(rs.getInt("poId"));
                po.setPoName(rs.getString("poName"));
                plo.setPloid(rs.getInt("ploId"));
                plo.setPloname(rs.getString("ploName"));
                m.setPo(po);
                m.setPlo(plo);
                mapping.add(m);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MappingDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mapping;
    }

    public ArrayList<Curriculum> getPODetailbyCode(String cid) {
        ArrayList<Curriculum> curriculumPOList = new ArrayList<>();
        try {
            String sql = "SELECT curriculum.curriculumid,curriculum.curriculumCode, curriculum.nameEn,\n"
                    + "po.poId,po.poName,po.poDecription\n"
                    + "FROM `flm`.`curriculum`\n"
                    + "LEFT JOIN PO ON po.curriculumId = curriculum.curriculumId\n"
                    + "where curriculum.curriculumId = (SELECT curriculumId FROM curriculum WHERE curriculumCode = ?);";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, cid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Curriculum c = new Curriculum();
                c.setCurriculumId(rs.getInt("curriculumid"));
                c.setCurriculumCode(rs.getString("curriculumCode"));
                c.setNameEn(rs.getString("nameEn"));

                PO po = new PO();
                po.setPoId(rs.getInt("poId"));
                po.setPoName(rs.getString("poName"));
                po.setPoDecription(rs.getString("poDecription"));
                c.setPo(po);

                curriculumPOList.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CurriculumDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return curriculumPOList;
    }

    public ArrayList<Curriculum> getPLODetailbyCode(String cid) {
        ArrayList<Curriculum> curriculumPLOList = new ArrayList<>();
        try {
            String sql = "SELECT curriculum.curriculumid, curriculum.curriculumCode, curriculum.nameEn,\n"
                    + "plo.ploid, plo.ploName, plo.ploDecription\n"
                    + "FROM `flm`.`curriculum`\n"
                    + "LEFT JOIN plo ON plo.curriculumId = curriculum.curriculumId\n"
                    + "where curriculum.curriculumId = (SELECT curriculumId FROM curriculum WHERE curriculumCode = ?);";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, cid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Curriculum c = new Curriculum();
                c.setCurriculumId(rs.getInt("curriculumid"));
                c.setCurriculumCode(rs.getString("curriculumCode"));
                c.setNameEn(rs.getString("nameEn"));

                PLO plo = new PLO();
                plo.setPloid(rs.getInt("ploid"));
                plo.setPloname(rs.getString("ploName"));
                plo.setPlodescription(rs.getString("ploDecription"));
                c.setPlo(plo);

                curriculumPLOList.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CurriculumDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return curriculumPLOList;
    }

    public ArrayList<Mapping> getMappingbyCode(String cid) {
        ArrayList<Mapping> mapping = new ArrayList<>();
        try {
            String sql = "SELECT plo.ploName,po.poName,plo.ploId,po.poId\n"
                    + "FROM mapping\n"
                    + "JOIN po ON mapping.poId = po.poId\n"
                    + "JOIN plo ON mapping.ploId = plo.ploId\n"
                    + "JOIN curriculum ON mapping.curriculumId = curriculum.curriculumId\n"
                    + "WHERE mapping.curriculumId = (SELECT curriculumId FROM curriculum WHERE curriculumCode = ?)\n"
                    + "Order by PO.poName";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, cid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Mapping m = new Mapping();
                PO po = new PO();
                PLO plo = new PLO();
                Curriculum c = new Curriculum();
                po.setPoId(rs.getInt("poId"));
                po.setPoName(rs.getString("poName"));
                plo.setPloid(rs.getInt("ploId"));
                plo.setPloname(rs.getString("ploName"));
                m.setPo(po);
                m.setPlo(plo);
                mapping.add(m);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MappingDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mapping;
    }

    public static void main(String[] args) {
        MappingDBContext d = new MappingDBContext();
        System.out.println(d.getPLODetailbyCode("BBA_MC_K16B"));
    }

    @Override
    public void insert(Mapping model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public void insertMapping(int poId, int ploId, int cur) {
        try {
            String sql = "insert into flm.mapping (poId, ploId, curriculumId)\n"
                    + "value (? , ?, ?);";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, poId);
            stm.setInt(2, ploId);
            stm.setInt(3, cur);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(CurriculumDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Mapping model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void delete(Mapping model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public void deleteMapping(int poId, int ploId) {
        try {
            String sql = "DELETE \n"
                    + "FROM flm.mapping \n"
                    + "where poId = ? AND ploId = ? ";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, poId);
            stm.setInt(2, ploId);
            stm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(CurriculumDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Mapping getStringId(String id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Mapping get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArrayList<Mapping> list() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
