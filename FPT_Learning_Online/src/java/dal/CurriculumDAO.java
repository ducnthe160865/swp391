/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Curriculum;

/**
 *
 * @author asus
 */
public class CurriculumDAO extends DBContext<Curriculum> {
    public List<Curriculum> sortDescBySid(int curriculumId) {
        List<Curriculum> list = new ArrayList<>();
        String sql = "Select * from curriculum\n"
                + "where curriculumId = ?\n"
                + "order by curriculumId desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, curriculumId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Curriculum s = new Curriculum();
                s.setCurriculumId(rs.getInt(1));
                s.setCurriculumCode(rs.getString(2));
                s.setNameEn(rs.getString(3));
                s.setDecription(rs.getString(4));
                s.setDecisionNo(rs.getString(5));            
                s.setTotalCredit(rs.getInt(6));
                s.setIsActive(rs.getBoolean(7));

                list.add(s);
            }
        } catch (Exception e) {
        }

        return list;
    }
      public List<Curriculum> sortAscBySid(int curriculumId) {
        List<Curriculum> list = new ArrayList<>();
        String sql = "Select * from curriculum\n"
                + "where curriculumId = ?\n"
                + "order by curriculumId asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, curriculumId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Curriculum s = new Curriculum();
                s.setCurriculumId(rs.getInt(1));
                s.setCurriculumCode(rs.getString(2));
                s.setNameEn(rs.getString(3));
                s.setDecription(rs.getString(4));
                s.setDecisionNo(rs.getString(5));            
                s.setTotalCredit(rs.getInt(6));
                s.setIsActive(rs.getBoolean(7));

                list.add(s);
            }
        } catch (Exception e) {
        }

        return list;
    }
    public List<Curriculum> getAll() {
        List<Curriculum> list = new ArrayList<>();

        try {
            String sql = "SELECT `curriculum`.`curriculumId`,\n"
                    + "    `curriculum`.`curriculumCode`,\n"
                    + "    `curriculum`.`nameEn`,\n"
                    + "    `curriculum`.`decription`,\n"
                    + "    `curriculum`.`decisionNo`,\n"
                    + "    `curriculum`.`totalCredit`,\n"
                    + "    `curriculum`.`isActive`\n"
                    + "FROM `flm`.`curriculum`;";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Curriculum decision = new Curriculum(rs.getInt("curriculumId"), rs.getString("curriculumCode"), rs.getString("nameEn"), rs.getString("decription"), rs.getString("decisionNo"), rs.getInt("totalCredit"), rs.getBoolean("isActive"));

                list.add(decision);
            }
        } catch (SQLException e) {
            System.out.println("decisionDAO, getDecisionByDecisionID" + e);
        }
        return list;
    }

    public List<Curriculum> listByPage2(List<Curriculum> selist, int start, int end) {
        List<Curriculum> list = new ArrayList<>();
        for (int i = start; i < end; i++) {
            list.add(selist.get(i));
        }
        return list;
    }

    public List<Curriculum> searchByCode(String txtSearch) {

        ArrayList<Curriculum> list = new ArrayList<>();
        String query = "SELECT * FROM flm.curriculum\n"
                + " where curriculumCode like '%" + txtSearch + "%' " + " or nameEn like '%" + txtSearch + "%' ORDER BY curriculumId";
        try {

            PreparedStatement st = connection.prepareStatement(query);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Curriculum(rs.getInt(1), rs.getString(2), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getBoolean(8))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
        public List<Curriculum> searchByCode1(String txtSearch) {

        ArrayList<Curriculum> list = new ArrayList<>();
        String query = "SELECT * FROM flm.curriculum\n"
                + " where curriculumCode like '%" + txtSearch + "%' " + " or nameEn like '%" + txtSearch + "%' ORDER BY curriculumId desc";
        try {

            PreparedStatement st = connection.prepareStatement(query);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Curriculum(rs.getInt(1), rs.getString(2), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getBoolean(8))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
        public List<Curriculum> searchByCode2(String txtSearch) {

        ArrayList<Curriculum> list = new ArrayList<>();
        String query = "SELECT * FROM flm.curriculum\n"
                + " where curriculumCode like '%" + txtSearch + "%' " + " or nameEn like '%" + txtSearch + "%' ORDER BY curriculumCode desc";
        try {

            PreparedStatement st = connection.prepareStatement(query);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Curriculum(rs.getInt(1), rs.getString(2), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getBoolean(8))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
                public List<Curriculum> searchByCode3(String txtSearch) {

        ArrayList<Curriculum> list = new ArrayList<>();
        String query = "SELECT * FROM flm.curriculum\n"
                + " where curriculumCode like '%" + txtSearch + "%' " + " or nameEn like '%" + txtSearch + "%' ORDER BY curriculumCode asc";
        try {

            PreparedStatement st = connection.prepareStatement(query);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Curriculum(rs.getInt(1), rs.getString(2), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getBoolean(8))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
                       public List<Curriculum> searchByCode4(String txtSearch) {

        ArrayList<Curriculum> list = new ArrayList<>();
        String query = "SELECT * FROM flm.curriculum\n"
                + " where curriculumCode like '%" + txtSearch + "%' " + " or nameEn like '%" + txtSearch + "%' ORDER BY decisionNo asc";
        try {

            PreparedStatement st = connection.prepareStatement(query);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Curriculum(rs.getInt(1), rs.getString(2), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getBoolean(8))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
                              public List<Curriculum> searchByCode5(String txtSearch) {

        ArrayList<Curriculum> list = new ArrayList<>();
        String query = "SELECT * FROM flm.curriculum\n"
                + " where curriculumCode like '%" + txtSearch + "%' " + " or nameEn like '%" + txtSearch + "%' ORDER BY decisionNo desc";
        try {

            PreparedStatement st = connection.prepareStatement(query);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Curriculum(rs.getInt(1), rs.getString(2), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getBoolean(8))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
   public List<Curriculum> getListByPage(List<Curriculum> list, int start, int end) {
        List<Curriculum> result = new ArrayList<>();

        for (int i = start; i < end; i++) {
            result.add(list.get(i));
        }
        return result;
    }
  public void addCurriculum(String curriculumCode, String nameEn,String decription, String decisionNo, boolean active){
       String sql = "INSERT INTO `flm`.`curriculum`\n"
                + "(\n"
                + "`curriculumCode`,\n"
                + "`nameEn`,\n"
                + "`decription`,\n"
                + "`decisionNo`,\n"
                + "`isActive`)\n"
                + "VALUES(?,?,?,?,?)";
       try {
          PreparedStatement st = connection.prepareStatement(sql);
st.setString(1,curriculumCode);
st.setString(2,nameEn);
st.setString(3,decription);
st.setString(4,decisionNo);
st.setBoolean(5,active);
st.executeUpdate();
      } catch (Exception e) {
      }
  }

    public Curriculum getCurriculumByCurriculumID(int curriculumId) {
        try {
            String sql = "SELECT `curriculum`.`curriculumId`,\n"
                    + "    `curriculum`.`curriculumCode`,\n"
                    + "    `curriculum`.`nameEn`,\n"
                    + "    `curriculum`.`decription`,\n"
                    + "    `curriculum`.`decisionNo`,\n"
                    + "    `curriculum`.`totalCredit`,\n"
                    + "    `curriculum`.`isActive`\n"
                    + "FROM `flm`.`curriculum`"
                    + "WHERE `curriculum`.`curriculumId` = ?;";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, curriculumId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Curriculum decision = new Curriculum(rs.getInt("curriculumId"), rs.getString("curriculumCode"), rs.getString("nameEn"), rs.getString("decription"), rs.getString("decisionNo"), rs.getInt("totalCredit"), rs.getBoolean("isActive"));
                return decision;
            }
        } catch (SQLException e) {
            System.out.println("decisionDAO, getDecisionByDecisionID" + e);
        }
        return null;
    }

    public void editCurriculum(Curriculum q) {
        String sql = "UPDATE `flm`.`curriculum`\n"
                + "SET\n"
                + "`curriculumCode` = ?,\n"
                + "`nameEn` = ?,\n"
                + "`decription` =?, \n"
                + "`isActive` = ?\n"
                + "WHERE `curriculumId` = ?;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, q.getCurriculumCode());
            st.setString(2, q.getNameEn());
            st.setString(3, q.getDecription());
            st.setBoolean(4, q.isIsActive());

            st.setInt(5, q.getCurriculumId());

            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public Curriculum getCurriculumByID(int curriculumId) {
        try {
            String sql = "SELECT *  FROM flm.curriculum "
                    + "Where curriculumId = ?;";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, curriculumId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Curriculum c = new Curriculum();
                c.setCurriculumId(rs.getInt(1));
                c.setCurriculumCode(rs.getString(4));
                c.setNameEn(rs.getString(3));
                c.setDecription(rs.getString(2));
                c.setIsActive(rs.getBoolean(17));
                return c;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public void insert(Curriculum model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void update(Curriculum model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void delete(Curriculum model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Curriculum getStringId(String id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Curriculum get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArrayList<Curriculum> list() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public static void main(String[] args) {
        CurriculumDAO dao = new CurriculumDAO();
        dao.addCurriculum("c", "c", "c", "1189/QÐ-ÐHFPT", true);
        List<Curriculum> list= dao.searchByCode("a");
        for (Curriculum curriculum : list) {
            System.out.println(curriculum.getNameEn());
        }
    }
}
