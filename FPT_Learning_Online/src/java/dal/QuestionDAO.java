/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import model.Question1;

import jakarta.servlet.jsp.jstl.sql.Result;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Decision;
import model.Question;
import model.Subject;
import model.Syllabus;
import model.Question1;
import model.Session;
import model.preRequisite;

/**
 *
 * @author asus
 */
public class QuestionDAO extends DBContext<Question1> {

    public List<Syllabus> searchByCodeSyllabus2(String txtSearch) {
        List<Syllabus> list = new ArrayList<>();
        String query = "SELECT a.syllabusId,a.subjectCode,a.syllabusNameEn,a.syllabusNameVn,a.isActive,a.isApproved,concat(a.decisionNo,' dated ',DATE_FORMAT(d.approvedDate,'%d/%m/%Y')),a.noCredit,a.degreeLevel,a.description\n"
                + "\n"
                + "FROM Syllabus a\n"
                + "\n"
                + "join Decision d on a.decisionNo=d.decisionNo \n"
                + "where a.subjectCode like '%" + txtSearch + "%' " + " or a.syllabusNameEn like '%" + txtSearch + "%'  order by syllabusId desc";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Syllabus(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getBoolean(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Syllabus> searchByCodeSyllabus4(String txtSearch) {
        List<Syllabus> list = new ArrayList<>();
        String query = "SELECT a.syllabusId,a.subjectCode,a.syllabusNameEn,a.syllabusNameVn,a.isActive,a.isApproved,concat(a.decisionNo,' dated ',DATE_FORMAT(d.approvedDate,'%d/%m/%Y')),a.noCredit,a.degreeLevel,a.description\n"
                + "\n"
                + "FROM Syllabus a\n"
                + "\n"
                + "join Decision d on a.decisionNo=d.decisionNo \n"
                + "where a.subjectCode like '%" + txtSearch + "%' " + " or a.syllabusNameEn like '%" + txtSearch + "%'  order by subjectCode desc";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Syllabus(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getBoolean(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Syllabus> searchByCodeSyllabus3(String txtSearch) {
        List<Syllabus> list = new ArrayList<>();
        String query = "SELECT a.syllabusId,a.subjectCode,a.syllabusNameEn,a.syllabusNameVn,a.isActive,a.isApproved,concat(a.decisionNo,' dated ',DATE_FORMAT(d.approvedDate,'%d/%m/%Y')),a.noCredit,a.degreeLevel,a.description\n"
                + "\n"
                + "FROM Syllabus a\n"
                + "\n"
                + "join Decision d on a.decisionNo=d.decisionNo \n"
                + "where a.subjectCode like '%" + txtSearch + "%' " + " or a.syllabusNameEn like '%" + txtSearch + "%'  order by subjectCode asc";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Syllabus(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getBoolean(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Syllabus> searchByCodeSyllabus5(String txtSearch) {
        List<Syllabus> list = new ArrayList<>();
        String query = "SELECT a.syllabusId,a.subjectCode,a.syllabusNameEn,a.syllabusNameVn,a.isActive,a.isApproved,concat(a.decisionNo,' dated ',DATE_FORMAT(d.approvedDate,'%d/%m/%Y')),a.noCredit,a.degreeLevel,a.description\n"
                + "\n"
                + "FROM Syllabus a\n"
                + "\n"
                + "join Decision d on a.decisionNo=d.decisionNo \n"
                + "where a.subjectCode like '%" + txtSearch + "%' " + " or a.syllabusNameEn like '%" + txtSearch + "%'  order by syllabusNameEn asc";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Syllabus(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getBoolean(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Syllabus> searchByCodeSyllabus6(String txtSearch) {
        List<Syllabus> list = new ArrayList<>();
        String query = "SELECT a.syllabusId,a.subjectCode,a.syllabusNameEn,a.syllabusNameVn,a.isActive,a.isApproved,concat(a.decisionNo,' dated ',DATE_FORMAT(d.approvedDate,'%d/%m/%Y')),a.noCredit,a.degreeLevel,a.description\n"
                + "\n"
                + "FROM Syllabus a\n"
                + "\n"
                + "join Decision d on a.decisionNo=d.decisionNo \n"
                + "where a.subjectCode like '%" + txtSearch + "%' " + " or a.syllabusNameEn like '%" + txtSearch + "%'  order by syllabusNameEn desc";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Syllabus(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getBoolean(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
   public List<Syllabus> getListByPage(List<Syllabus> list, int start, int end) {
        List<Syllabus> result = new ArrayList<>();

        for (int i = start; i < end; i++) {
            result.add(list.get(i));
        }
        return result;
    }
      public List<Question1> getListByPage1(List<Question1> list, int start, int end) {
        List<Question1> result = new ArrayList<>();

        for (int i = start; i < end; i++) {
            result.add(list.get(i));
        }
        return result;
    }
    public List<Syllabus> searchByCodeSyllabus(String txtSearch) {
        List<Syllabus> list = new ArrayList<>();
        String query = "SELECT a.syllabusId,a.subjectCode,a.syllabusNameEn,a.syllabusNameVn,a.isActive,a.isApproved,concat(a.decisionNo,' dated ',DATE_FORMAT(d.approvedDate,'%d/%m/%Y')),a.noCredit,a.degreeLevel,a.description\n"
                + "\n"
                + "FROM Syllabus a\n"
                + "\n"
                + "join Decision d on a.decisionNo=d.decisionNo \n"
                + "where a.subjectCode like '%" + txtSearch + "%' " + " or a.syllabusNameEn like '%" + txtSearch + "%'  order by syllabusId ";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Syllabus(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getBoolean(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public void deleteQuestion(int qid) {
        String sql = "DELETE FROM `flm`.`question`\n"
                + "WHERE `questionId` = ?;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, qid);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public List<Question1> searchByCodeQuestion(String txtSearch) {
        List<Question1> list = new ArrayList<>();
        String query = "SELECT a.questionId,a.name,a.detail,a.sessionId,a.syllabusId\n"
                + "FROM flm.question a\n"
                + "where a.name like '%" + txtSearch + "%' " + " or a.detail like '%" + txtSearch + "%'  order by questionId";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Question1(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
    public List<Question1> searchByCodeQuestion1(String txtSearch) {
        List<Question1> list = new ArrayList<>();
        String query = "SELECT a.questionId,a.name,a.detail,a.sessionId,a.syllabusId\n"
                + "FROM flm.question a\n"
                + "where a.name like '%" + txtSearch + "%' " + " or a.detail like '%" + txtSearch + "%'  order by questionId desc";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Question1(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
        public List<Question1> searchByCodeQuestion2(String txtSearch) {
        List<Question1> list = new ArrayList<>();
        String query = "SELECT a.questionId,a.name,a.detail,a.sessionId,a.syllabusId\n"
                + "FROM flm.question a\n"
                + "where a.name like '%" + txtSearch + "%' " + " or a.detail like '%" + txtSearch + "%'  order by sessionId asc";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Question1(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
            public List<Question1> searchByCodeQuestion3(String txtSearch) {
        List<Question1> list = new ArrayList<>();
        String query = "SELECT a.questionId,a.name,a.detail,a.sessionId,a.syllabusId\n"
                + "FROM flm.question a\n"
                + "where a.name like '%" + txtSearch + "%' " + " or a.detail like '%" + txtSearch + "%'  order by sessionId desc";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Question1(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
            public List<Question1> searchByCodeQuestion4(String txtSearch) {
        List<Question1> list = new ArrayList<>();
        String query = "SELECT a.questionId,a.name,a.detail,a.sessionId,a.syllabusId\n"
                + "FROM flm.question a\n"
                + "where a.name like '%" + txtSearch + "%' " + " or a.detail like '%" + txtSearch + "%'  order by detail asc";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Question1(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
            public List<Question1> searchByCodeQuestion5(String txtSearch) {
        List<Question1> list = new ArrayList<>();
        String query = "SELECT a.questionId,a.name,a.detail,a.sessionId,a.syllabusId\n"
                + "FROM flm.question a\n"
                + "where a.name like '%" + txtSearch + "%' " + " or a.detail like '%" + txtSearch + "%'  order by detail desc";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Question1(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
    public void deleteSession(int sessionId) {
        String sql = "DELETE FROM `flm`.`question`\n"
                + "WHERE `sessionId` = ?;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, sessionId);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public Question1 getQuestionByQuestionID(int questionId) {
        try {
            String sql = "SELECT `question`.`questionId`,\n"
                    + "    `question`.`name`,\n"
                    + "    `question`.`detail`,\n"
                    + "    `question`.`sessionId`,\n"
                    + "    `question`.`syllabusId`\n"
                    + "FROM `flm`.`question`"
                    + "WHERE `question`.`questionId` = ?;";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Question1 decision = new Question1(rs.getInt("questionId"), rs.getString("name"), rs.getString("detail"), rs.getInt("sessionId"), rs.getInt("syllabusId"));
                return decision;
            }
        } catch (SQLException e) {
            System.out.println("decisionDAO, getDecisionByDecisionID" + e);
        }
        return null;
    }

    public void editQuestion(Question1 q) {
        String sql = "UPDATE `flm`.`question`\n"
                + "SET\n"
                + "`name` = ?,\n"
                + "`detail` =? \n"
                + "WHERE `questionId` = ?;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, q.getName());
            st.setString(2, q.getDetail());
            st.setInt(3, q.getQuestionId());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void insertQuestion(int syllabusId, String name, String detail) {
        String sql = "INSERT INTO `flm`.`question`\n"
                + "(\n"
                + "`syllabusId`,\n"
                + "`name`,\n"
                + "`detail`)\n"
                + "VALUES\n"
                + "(?,?,?);";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, syllabusId);
            st.setString(2, name);
            st.setString(3, detail);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public List<Question1> getQuestionBySyllabusID(int syllabusId) {
        List<Question1> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM flm.question\n"
                    + "where syllabusId = ?\n"
                    + "order by questionId asc;";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, syllabusId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Question1 s = new Question1();
                s.setQuestionId(rs.getInt(1));
                s.setName(rs.getString(2));
                s.setDetail(rs.getString(3));
                s.setSessionId(rs.getInt(4));
                s.setSyllabusId(rs.getInt(5));
                list.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    public List<Syllabus> getAll() {
        List<Syllabus> list = new ArrayList<>();

        try {
            String sql = "SELECT `syllabus`.`syllabusId`,\n"
                    + "    `syllabus`.`subjectCode`,\n"
                    + "    `syllabus`.`syllabusNameEn`,\n"
                    + "    `syllabus`.`syllabusNameVn`,\n"
                    + "    `syllabus`.`isApproved`,\n"
                    + "    `syllabus`.`isActive`,\n"
                    + "    `syllabus`.`decisionNo`,\n"
                    + "    `syllabus`.`noCredit`,\n"
                    + "    `syllabus`.`degreeLevel`,\n"
                    + "    `syllabus`.`description`\n"
                    + "FROM `flm`.`syllabus`;";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Syllabus decision = new Syllabus(rs.getInt("syllabusId"), rs.getString("subjectCode"), rs.getString("syllabusNameEn"), rs.getString("syllabusNameVn"), rs.getBoolean("isApproved"), rs.getBoolean("isActive"), rs.getString("decisionNo"), rs.getInt("noCredit"), rs.getString("degreeLevel"), rs.getString("description"));

                list.add(decision);
            }
        } catch (SQLException e) {
            System.out.println("decisionDAO, getDecisionByDecisionID" + e);
        }
        return list;
    }

    @Override
    public void insert(Question1 model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void update(Question1 model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void delete(Question1 model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Question1 getStringId(String id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Question1 get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArrayList<Question1> list() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<Question1> listByPage(List<Question1> selist, int start, int end) {
        List<Question1> list = new ArrayList<>();
        for (int i = start; i < end; i++) {
            list.add(selist.get(i));
        }
        return list;
    }

    public List<Syllabus> listByPage2(List<Syllabus> selist, int start, int end) {
        List<Syllabus> list = new ArrayList<>();
        for (int i = start; i < end; i++) {
            list.add(selist.get(i));
        }
        return list;
    }

}
