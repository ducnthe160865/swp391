/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.syllabus;


import dal.SyllabusDBContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import model.Syllabus;
import sort.SortPage;


/**
 *
 * @author asus
 */
@WebServlet(name = "SyllabusServlet", urlPatterns = {"/FLM/syllabus"})
public class SyllabusServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SyllabusServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SyllabusServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String txt;
        if (request.getParameter("txt") != null) {
            txt = request.getParameter("txt");
        } else if (request.getParameter("page") != null) {
            txt = "";
        } else {
            txt = "sdfsdfsdfsdf";
        }

       SyllabusDBContext db = new  SyllabusDBContext();
        String sort = request.getParameter("sort");
        String xpage = request.getParameter("page");
        if (sort == null) {
            sort = "idasc";
        }
        if (sort.equals("idasc")) {

            List< Syllabus> list = db.searchByCodeSyl(txt);
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp=new SortPage();
            int[] arr=sp.getStartEnd(list.size(), xpage);
            List< Syllabus> listByPage = db.getListByPage(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);

        } else if (sort.equals("iddesc")) {
            List< Syllabus> list = db.searchByCodeSyl1(txt);
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp=new SortPage();
            int[] arr=sp.getStartEnd(list.size(), xpage);
            List< Syllabus> listByPage = db.getListByPage(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);
        }else if (sort.equals("codedesc")) {
            List<Syllabus> list = db.searchByCodeSyl2(txt);
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp=new SortPage();
            int[] arr=sp.getStartEnd(list.size(), xpage);
            List<Syllabus> listByPage = db.getListByPage(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);
        }else if (sort.equals("codeasc")) {
            List<Syllabus> list = db.searchByCodeSyl3(txt);
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp=new SortPage();
            int[] arr=sp.getStartEnd(list.size(), xpage);
            List<Syllabus> listByPage = db.getListByPage(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);
        }else if (sort.equals("typedesc")) {
            List<Syllabus> list = db.searchByCodeSyl4(txt);
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp=new SortPage();
            int[] arr=sp.getStartEnd(list.size(), xpage);
            List<Syllabus> listByPage = db.getListByPage(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);
        }else if (sort.equals("typeasc")) {
            List<Syllabus> list = db.searchByCodeSyl5(txt);
            if (list.isEmpty() && request.getParameter("txt") != null) {
                String message = "Subject code " + "<span class='text-danger'>" + txt + "</span>" + " does not exist or has no syllabus";
                request.setAttribute("message", message);
            }
            SortPage sp=new SortPage();
            int[] arr=sp.getStartEnd(list.size(), xpage);
            List<Syllabus> listByPage = db.getListByPage(list, arr[0], arr[1]);
            request.setAttribute("list", listByPage);
            request.setAttribute("page", arr[2]);
            request.setAttribute("numberOfPage", arr[3]);
        }
        
        request.setAttribute("sort", sort);
        if (!txt.equalsIgnoreCase("sdfsdfsdfsdf")) {
            request.setAttribute("txt", txt);
        }
        request.getRequestDispatcher("/view/syllabus/syllabus.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String txt = request.getParameter("txt");
        SyllabusDBContext syl = new SyllabusDBContext();
        List<Syllabus> list = syl.searchByCodeSyl(txt);
//        List<Sub> list2 = syl.searchByNameSyl(txt);

        if (list.isEmpty()) {
            String message = "Syllabus " + txt + " does not exist!";
            request.setAttribute("message", message);
        }

        request.setAttribute("txt", txt);
        request.setAttribute("list", list);

        SyllabusDBContext sDao = new SyllabusDBContext();
        String syllabusID_raw = request.getParameter("syllabusId");
        String indexPage_raw = request.getParameter("indexPage");
        List<Syllabus> selist = sDao.getAllSyllabus();
        int numPerPage = 4;
        int size = selist.size();
        int page = 1;

        if (indexPage_raw == null) {
            page = 1;
        } else {
            try {
                page = Integer.parseInt(indexPage_raw);
            } catch (Exception e) {
            }
        }
        int numberOfPage = (size % numPerPage == 0 ? size / numPerPage : size / numPerPage + 1);
        int start, end;
        start = (page - 1) * numPerPage;
        end = Math.min(page * numPerPage, size);
        List<Syllabus> listByPage = sDao.listByPage2(selist, start, end);

        request.setAttribute("listsy", listByPage);
        request.setAttribute("start", start);
        request.setAttribute("end", end);
        request.setAttribute("page", page);
        request.setAttribute("numpage", numberOfPage);
        request.setAttribute("syllabusId", syllabusID_raw);
        request.setAttribute("txt", txt);
        request.setAttribute("list", list);

        request.getRequestDispatcher("/view/syllabus/syllabus.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}