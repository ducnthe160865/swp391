/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.admin.po_plo;

import dal.MappingDBContext;
import dal.PoDBContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import model.Curriculum;

/**
 *
 * @author l
 */
public class Mapping extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int plo = Integer.parseInt(req.getParameter("plo"));
        int po = Integer.parseInt(req.getParameter("po"));
        boolean check = Boolean.parseBoolean(req.getParameter("check"));
        String crun = req.getParameter("cru");
        if (check) {
            new MappingDBContext().deleteMapping(po, plo);
        } else {
            int cru = new MappingDBContext().GetIdCurriculumByPOCode(crun);
            new MappingDBContext().insertMapping(po, plo, cru);
        }
        resp.sendRedirect("/FPT_Learning_Online/FLM/mapping?cid="+crun);

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String cid = req.getParameter("cid");
        MappingDBContext map = new MappingDBContext();
        List<Curriculum> plolist = map.getPLODetailbyCode(cid);
        List<Curriculum> polist = map.getPODetailbyCode(cid);

        req.setAttribute("plolist", plolist);
        req.setAttribute("polist", polist);

        List<model.Mapping> mapping = map.getMappingbyCode(cid);
        if (mapping.isEmpty()) {
            String message = "Name = " + cid + " does not exist!";
            req.setAttribute("message", message);
        }

        req.setAttribute("cuId", cid);
        req.setAttribute("mapping", mapping);
        // lấy name curriculumn
        req.getRequestDispatcher("/view/admin/POPLO/Mapping.jsp").forward(req, resp);
    }

}
